﻿using System;
using Xwt;

namespace mdedit
{
    class Program
    {
        [STAThread]
        public static void Main(string[] args)
        {
            if (RuntimeInfo.IsLinux)
                Application.Initialize(ToolkitType.Gtk);
            else if (RuntimeInfo.IsMacOSX)
                Application.Initialize(ToolkitType.Gtk);
            else if (RuntimeInfo.IsWindows)
                Application.Initialize(ToolkitType.Wpf);

            var service = new MDService();
            var window = new MDWindow(service, args);
            window.Closed += (sender, e) => Application.Exit();
            window.Show();
            Application.Run();
            window.Dispose();
        }
    }
}