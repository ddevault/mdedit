# mdedit

mdedit is a self hosting desktop markdown editor. *(Self hosting means that I used it to write this readme)*

![](http://a.pomf.se/uitrha.png)

It does not have a very imaginative name. It also does not have an icon, and you should totally make one and send it to me so it can look less bland.

**This project is not done**.

I know it'll work on Linux and Windows but I haven't tried it on OS X. Let me know how it goes if you can get it working.

## Downloading

*"I can't be arsed to compile this*"

1. [Download it](https://github.com/SirCmpwn/mdedit/releases)
2. Good work, you're done

## Compiling

This depends on:

* nuget
* mono or Microsoft.NET

With MS.NET, open the solution with Visual Studio or whatever and just build it. With Mono, do this stuff:

    $ mozroots --import --sync
    $ nuget restore
    $ xbuild

Then you'll have binaries in `bin/Debug`. You can build in release mode by passing `/p:Configuration=Release` to xbuild but it doesn't really matter all that much. On Linux and OS X, when you want to run the software, use this command:

    $ mono mdedit.exe [files...]

A simple bash script in `/usr/bin/local/` will give you support for running it via `mdedit [files...]`:

```bash
#!/bin/bash
# Obviously you need to install mdedit to /opt/mdedit/ for this to work
mono /opt/mdedit/mdedit.exe $*
```

## Contributing

Send a pull request.

## Bugs

Yes.

## Acknowledgements

This is just an [Xwt](https://github.com/mono/xwt) GUI that exposes [this cool markdown editor](https://github.com/jbt/markdown-editor) through a web view, and uses web sockets and other junk to communicate with the host window. It's pretty simple, most of the work was done by [@jbt](https://github.com/jbt).
