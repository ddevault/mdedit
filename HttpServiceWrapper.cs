﻿using System;
using System.Net;
using Griffin.Networking.Buffers;
using Griffin.Networking.Protocol.Http;
using Griffin.Networking.Protocol.Http.Protocol;
using Griffin.Networking.Servers;
using System.IO;
using System.Text;

namespace mdedit
{
    public class HttpServiceWrappper : HttpService
    {
        public class ServiceFactory : IServiceFactory
        {
            public RequestHandler Request;

            public ServiceFactory(RequestHandler request)
            {
                Request = request;
            }

            public INetworkService CreateClient(EndPoint remoteEndPoint)
            {
                return new HttpServiceWrappper(Request);
            }
        }

        private static readonly BufferSliceStack stack = new BufferSliceStack(50, 32000);

        public delegate IResponse RequestHandler(IRequest request);
        public RequestHandler Request;

        public HttpServiceWrappper(RequestHandler request)
            : base(stack)
        {
            Request = request;
        }

        public override void Dispose()
        {
        }

        public override void OnRequest(IRequest request)
        {
            var updated = Request(request);
            Send(updated);
        }
    }
}